/*
 * Taghreed Safaryan
 *  991494905
 * 
 */

/**
 *
 * @author Taghreed Safaryan
 */
public class bankemployee {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       Bank bank = new Bank("BMO");
       Employee emp = new Employee("Alex");
       System.out.println("Bank Name: "+ bank.getBankName()+ " \t " + "Employee Name: " + emp.getEmpName());
    }
    
}
